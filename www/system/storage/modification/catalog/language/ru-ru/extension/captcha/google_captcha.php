<?php
// *	@source		See SOURCE.txt for source and other copyright.
// *	@license	GNU General Public License version 3; see LICENSE.txt

// Heading
$_['heading_title'] = 'Капча';

// Entry
$_['entry_captcha'] = 'Код с картинки';

// Error
$_['error_captcha'] = 'Неверно введен код с картинки!';
